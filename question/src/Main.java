import Question1.*;
import Question2.*;

public class Main {
    public static void main(String[] args) throws Exception {
        //Question1

//        문제1
//        Question1.HelloBiodome01.input(args);

        //문제2 보너스있음
        /*double sum = 0;
        double sun = 0;
        double wind = 0;
        double earth = 0;
        sum = Question1.HelloBiodome02.sum(args);
        if (sum == 0) {
            System.out.println("값오류");
        }
        else {
            if (args.length > 0) {
                sun = Integer.parseInt(args[0]) / sum * 100;
                wind = Integer.parseInt(args[1]) / sum * 100;
                earth = Integer.parseInt(args[2]) / sum * 100;
            }
            System.out.println("총 에너지 생산량은 " + sum + "입니다.");
            System.out.printf("태양광 %f%s, 풍력 %f%s, 지열 %f%s", sun, "%", wind, "%", earth, "%");
        }*/

        //문제3
//        double H = 0;
//        H = Question1.HelloBiodome03.lifeValue(args);
//        if (H == -1)
//            System.out.println("입력된 값이 올바르지 않습니다. [온도][습도][산소농도] 순서 대로 숫자 값을 입력해주세요.");
//        else
//            System.out.printf("생명지수 H = %.2f" , H);

        /*//문제4 보너스존재
        int result = 0;
        result = Question1.HelloBiodome04.checkValue(args);

        if (result == 0) {
            double H = 0;
            H = Question1.HelloBiodome03.lifeValue(args);
            System.out.printf("생명의 나무는 안정적인 상태입니다. 건강지수는 %f.3입니다.\n", H);
        }
        else if (result == 1)
            System.out.println("온도값이 정상 범위를 벗어났습니다. 확인이 필요합니다.");
        else if (result == 2)
            System.out.println("습도값이 정상 범위를 벗어났습니다. 확인이 필요합니다.");
        else if (result == 3)
            System.out.println("산도농도값이 정상 범위를 벗어났습니다. 확인이 필요합니다.");
        else if (result == -1)
            System.out.println("입력 값의 개수가 부족합니다. [온도][습도][산소농도] 순서대로 숫자 값을 입력해주세요.");
        else if (result == -2)
            System.out.println("입력 값이 올바르지 않습니다. [온도][습도][산소농도] 순서대로 숫자 값을 입력해주세요.");
*/
        /*//문제5
        int result = 0;
        result = Question1.HelloBiodome05.biodome5();
        if (result == -1)
            System.out.println("존재하지 않는 답입니다.");
        else
            System.out.println("(h*h+g) * (h<<h) + (g<<g) = " + result);*/

        /*//문제6
        int result;
        result = Question1.HelloBiodome06.check_gene(args);

        if (result == -1)
            System.out.println("두 개의 유전자 코드를 입력해주세요.");
        else if (result == -2)
            System.out.println("유전자 코드의 개수는 5개 이상 20개 이하입니다.");
        else if (result == 1)
            System.out.println("일치하지 않습니다.");
        else if (result == 2)
            System.out.println("동일한 유전자 코드입니다.");
        else
            System.out.println("시스템 에러가 발생");*/

        /*//문제7
        String result;
        result = Question1.HelloBiodome07.compress(args);
        System.out.println(result);*/

        //문제8
//        String result;
//        result = Question1.HelloBiodome08.aaa(args);
//        System.out.println(result);

        //문제9 추가문제있음
//        Question1.HelloBiodome09.draw_tree(args);


        //Question2
        //문제1
//        String result;
//        result = RoadToBiodome01.check_wave(args);
//        System.out.println(result);

        //문제2
//        String result;
//        result = RoadToBiodome02.reverse_string(args);
//        System.out.println(result);

        //문제4
//        RoadToBiodome04.sort(args);

        //문제5
//        int[] result;
//        result = RoadToBiodome05.quicksort(args);
//        if (result == null) {
//            return;
//        }
//        for (int value : result) {
//            System.out.print(value + " ");
//        }

        //문제6
//        float[] result;
//        result = RoadToBiodome06.meanMedian(args);
//        System.out.printf("%.1f %.1f\n", result[0], result[1]);

        //문제7
//        String[] result;
//        result = RoadToBiodome07.sortAnimal(args);
//        for (int i = 0; i < result.length; i++) {
//            if (result[i] != null)
//                System.out.print(result[i] + " ");
//        }

        //문제8
//        RoadToBiodome08.Queue(args);

        //문제9
//        RoadToBiodome09.FindRoad(args);

        //문제10
        RoadToBiodome10.matchPlant(args);
    }
}