package Question1;

public class HelloBiodome01 {
    public static void input(String[] args) {
        if (args.length != 0) {
            String name = "";
            for (String str : args) {
                name += str + " ";
            }
            name = name.trim();

            System.out.println("안녕하세요, " + name + "님!");
        }
        else {
            System.out.println("이름을 입력해주세요!");
        }
    }
}
