package Question1;

public class HelloBiodome02 {
    public static double sum(String[] args) {
        if (args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                if (Double.parseDouble(args[i]) > 30000) {
                    return 0;
                }
            }

            double sun = 0;
            double earth = 0;
            double wind = 0;
            double sum = 0;
            sun = Double.parseDouble(args[0]);
            wind = Double.parseDouble(args[1]);
            earth = Double.parseDouble(args[2]);

            sum = sun + earth + wind;

            return sum;
        }
        else {
            return 0;
        }
    }
}
