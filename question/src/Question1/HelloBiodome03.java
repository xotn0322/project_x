package Question1;

public class HelloBiodome03 {
    public static double lifeValue (String[] args) {
        if (args.length != 0) {
            double temp = 0;
            double Humidity = 0;
            double Oxygen = 0;
            double result = 0;
            final double pie = 3.14;

            for (int i = 0; i < args.length; i++) {
                if (!isNumberic(args[i])) {
                    return -1;
                }
            }

            temp = Double.parseDouble(args[0]);
            Humidity = Double.parseDouble(args[1]);
            Oxygen = Double.parseDouble(args[2]);

            result = 0.415 * abs(root(Humidity) - temp) + (Oxygen / pow(pie));

            return result;
        }
        else {
            return -1;
        }
    }

    static double root(double Humidity) { //루트 계산식
        double epsilon = 0.00000001;
        double guess = Humidity / 2;

        while (true) {
            double newGuess = (guess + Humidity / guess) / 2;
            if (abs(newGuess - guess) < epsilon) {
                break;
            }
            guess = newGuess;
        }

//        System.out.println(guess);
//        System.out.println(Math.sqrt(Humidity));

        return guess;
    }

    static double pow(double A) { //제곱
        double result = 0;
        result = A * A;

        return result;
    }

    static double abs(double A) {
        if (A > 0)
            return A;
        else if (A < 0)
            return A * -1;
        else
            return 0;
    }

    public static boolean isNumberic(String str) {
        return str.matches("[+-]?\\d*(\\.\\d+)?");
    }
}
