package Question1;

public class HelloBiodome04 {
    public static int checkValue(String[] args) {
        if (args.length == 0) {
            return -1; //입력 개수 이상
        }
        else if (!isNumberic(args[0]) || !isNumberic(args[1]) || !isNumberic(args[2])) {
            return -2; //입력값 이상
        }
        else {
            double temp = 0;
            double Humidity = 0;
            double Oxygen = 0;

            boolean t_check = false;
            boolean h_check = false;
            boolean o_check = false;

            temp = Double.parseDouble(args[0]);
            Humidity = Double.parseDouble(args[1]);
            Oxygen = Double.parseDouble(args[2]);

            t_check = checkTemp(temp);
            h_check = checkHumidity(Humidity);
            o_check = checkOxygen(Oxygen);

            if (!t_check)
                return 1; //온도이상
            if (!h_check)
                return 2; //습도이상
            if (!o_check)
                return 3; //공기이상

            return 0; //문제없음
        }
    }

    static boolean checkTemp(double temp) {
        if (temp >= 10 && temp < 27.5) {
            return true;
        }
        else
            return false;
    }

    static boolean checkHumidity(double Humidity) {
        if (Humidity > 40 && Humidity < 60) {
            return true;
        }
        else
            return false;
    }

    static boolean checkOxygen(double Oxygen) {
        if (Oxygen >= 19.5 && Oxygen <= 23.5) {
            return true;
        }
        else
            return false;
    }

    public static boolean isNumberic(String str) {
        return str.matches("[+-]?\\d*(\\.\\d+)?");
    }
}
