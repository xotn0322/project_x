package Question1;

public class HelloBiodome05 {
    public static int biodome5 () {
        int g, h;
        int result = -1;

        for (g = 0; g <= 0b1111; g++) {
            for (h = 0; h <= 0b1111; h++) {
                if (solve1(g, h) && solve2(g, h)) {
                    result =  solve3(g, h);
                }
            }
        }
        return result;
    }

    static boolean solve1 (int g, int h) {
        return (g & 1 >> g << 2 | h + g ^ h) == 1;
    }
    static boolean solve2 (int g, int h) {
        return (g % 2 << h >> g | 1 & 0 ^ 0) == 2;
    }
    static int solve3 (int g, int h) {
        return (h * h + g) * (h << h) + (g << g);
    }
}
