package Question1;

public class HelloBiodome06 {
    public static int check_gene (String[] args) { //-1, -2, 1, 2
        if (args.length < 2)
            return -1; //개수 이상
        else {
            for (int i = 0; i < args.length; i++) {
                if (args[i].length() < 5 || args[i].length() > 20)
                    return -2; //유전자 정보 개수 이상
            }
            String gene1, gene2;

            gene1 = args[0];
            gene2 = args[1];

            if (gene1.length() == gene2.length()) {
                char[] gene1_c = gene1.toCharArray();
                char[] gene2_c = gene2.toCharArray();
                for (int i = 0; i < gene1.length(); i++) {
                    if (gene1_c[i] != gene2_c[i]) {
                        return 1;
                    }
                }
                return 2; //같다.
            }
            else {
                return 1; //다르다.
            }
        }
    }
}
