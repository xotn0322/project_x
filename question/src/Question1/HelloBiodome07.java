package Question1;

public class HelloBiodome07  {
    public static String compress(String[] args) {

        String compressed = "";
        String result = "";
        int count = 1;
        char select;

        if (args.length != 0) { //args 합치기 및 공백제거, 대문자화
            for (String str : args) {
                compressed += str;
                compressed = compressed.trim();
            }
            compressed = compressed.toUpperCase();
        }
        else
            return "DNA를 입력해주세요.";

        select = compressed.charAt(0); //첫번째 글자 선택

        for (int i = 1; i < compressed.length(); i++) {
            char next = compressed.charAt(i);
            if (select == next)
                count += 1;
            else {
                if (!check(select)) {
                    return "염기서열은 C, J, H, E, Y 다섯가지로만 입력됩니다. 확인하고 다시 입력해주세요.";
                }
                result += select + Integer.toString(count);
                count = 1;
                select = next;
            }
        }

        if (!check(select)) {
            return "염기서열은 C, J, H, E, Y 다섯가지로만 입력됩니다. 확인하고 다시 입력해주세요.";
        }
        result += select + Integer.toString(count);

        return result;
    }

    static boolean check(char select) {
        return select == 'C' || select == 'J' || select == 'H' || select == 'E' || select == 'Y';
    }
}
