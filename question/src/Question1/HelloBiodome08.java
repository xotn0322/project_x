package Question1;

import java.util.Arrays;

public class HelloBiodome08 {
    static String[] dictionary = {"hello", "where", "this", "biodome", "help", "tree", "new", "is", "problem", "please", "need", "we", "isn’t", "there", "a", "your", "any", "thanks", "the", "for", "solution", "can", "you", "?"};
    static StringBuilder stringbuilder;
    public static String aaa(String[] args) {
        if (args.length <= 0) {
            return "텍스트를 입력해주세요.";
        }
        else {
            //init
            stringbuilder = new StringBuilder();
            String result;
            Arrays.sort(dictionary,(String s1, String s2) -> s2.length() - s1.length());

            //input args to builder
            for (int i = 0; i < args.length; i++) {
                stringbuilder.append(args[i]);
            }

            //
            check_nbsp(stringbuilder);

            if (stringbuilder.charAt(stringbuilder.length() - 2) == '?') {
                stringbuilder.deleteCharAt(stringbuilder.length() - 3);
            } else {
                stringbuilder.deleteCharAt(stringbuilder.length() - 1);
                stringbuilder.append('.');
            }

            return stringbuilder.toString();
        }
    }

    static void check_nbsp(StringBuilder input) {
        for (int i = 0; i < input.length(); i++) {
            for (String word : dictionary) {
                if (i + word.length() <= input.length() && input.substring(i, i + word.length()).equals(word)) {
                    input.insert(i + word.length(), ' ');
                    if (i - 1 >= 0)
                        if (input.charAt(i - 1) != ' ' && input.charAt(i) != '?')
                            input.insert(i, ' ');
                    i += word.length();
                    break;
                }
            }
        }
    }
}
