package Question1;

public class HelloBiodome09 extends HelloBiodome04{
    static public void draw_tree(String[] args) {
        if (args.length <= 0) {
            System.out.println("값을 입력해주세요.");
        } else {
            if (!isNumberic(args[0]) || Integer.parseInt(args[0]) < 3 || Integer.parseInt(args[0]) > 100) {
                System.out.println("잘못된 입력입니다. 3~100 사이의 숫자를 입력하세요.");
            } else {
                //init
                int height = Integer.parseInt(args[0]);
                String decoration = args[1];

                //draw tree
                for (int i = 1; i <= height; i++) {
                    //왼쪽 빈공간
                    for (int j = height; j > i; j--) {
                        System.out.print("  ");
                    }
                    //왼쪽부분
                    for (int j = 0; j < (i - 1); j++) {
                        System.out.print("* ");
                    }
                    //중심
                    System.out.print(decoration + " ");

                    //오른쪽 부분
                    for (int j = 0; j < i - 1; j++) {
                        System.out.print("* ");
                    }

                    System.out.println();
                }
                //기둥 위치잡기
                for (int i = height; i > 1; i--) {
                    System.out.print("  ");
                }
                System.out.print("|");
            }
        }
    }
}
