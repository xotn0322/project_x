package Question2;

import Question1.HelloBiodome04;

import java.util.ArrayList;

public class RoadToBiodome01 extends HelloBiodome04 {
    static ArrayList<Integer> waves;
    static final int max_size = 1000;
    static final int min_size = 0;
    static public String check_wave(String[] args) {
        if (args.length <= 0) {
            return "값을 입력해주세요.";
        }
        else {
            //init
            waves = new ArrayList<Integer>();
            ArrayList<Integer> result;

            //값 집어넣기
            for (int i = 0; i < args.length; i++) {
                if (isNumberic(args[i])) {
                    if (Integer.parseInt(args[i]) > max_size || Integer.parseInt(args[i]) < min_size || !isNumberic(args[i])) {
                        return "입력된 값의 범위가 올바르지 않습니다. 0에서 1000까지의 값을 입력해주세요.";
                    } else {
                        waves.add(Integer.parseInt(args[i]));
                    }
                }
                else {
                    return "입력된 값의 범위가 올바르지 않습니다. 0에서 1000까지의 값을 입력해주세요.";
                }
            }

            result = check_duplication(waves);

            return result.toString();
        }
    }

    static ArrayList<Integer> check_duplication(ArrayList<Integer> waves) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        int[] dup = new int[max_size];

        for (int wave : waves) {
            dup[wave]++;
        }

        for (int wave : waves) {
            if (dup[wave] == 1) {
                result.add(wave);
            }
        }

        return result;
    }
}
