package Question2;

public class RoadToBiodome02 {
    static char[] data = new char[1000000];
    static String result = "";
    static int top = -1;

    public static String reverse_string(String[] args) {
        if (args.length > 0) {

            //init
            String input = "";
            char output;

            for (String str : args) {
                input += str + " ";
            }
            input = input.trim();

            //입력
            for (int i = 0; i < input.length(); i++) {
                m_push(input.charAt(i));
            }

            int d_length = data.length;

            //출력
            for (int i = 0; i < data.length; i++) {
                output = m_pop();
                if (output != '*') {
                    result += output;
                }
            }

            return result;
        }
        else {
            return "텍스트를 입력해주세요.";
        }
    }

    static void m_push(char value) {
        top++;
        data[top] = value;
    }

    static char m_pop() {
        char value;
        if (m_isEmpty()) {
//            System.out.println("비어있습니다.");
            return '*';
        }
        else {
            value = data[top];
            top--;
            return value;
        }
    }

    static char m_peek() {
        if (m_isEmpty()) {
            System.out.println("비어있습니다.");
            return '*';
        }
        else {
            return data[top];
        }
    }

    static boolean m_isEmpty() {
        if (top <= -1) {
            return true;
        }
        else {
            return false;
        }
    }
}
