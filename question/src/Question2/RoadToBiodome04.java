package Question2;

public class RoadToBiodome04 {
    public static void sort(String[] args) {
        if (args.length > 0) {
            //init
            String input = "";
            int[] parse = {};
            double median = 0;
            double average = 0;

            //input args in input
            for (String str : args) {
                input += str + " ";
            }
            input = input.trim();

            parse = parseArray(input);
            sortArray(parse);
            average = calculateAverage(parse);
            median = calculateMedian(parse);

            System.out.println("평균값 : " + average + ", 중앙값 : " + median);
        }
        else {
            return;
        }
    }

    private static int[] parseArray(String input) {
        // 대괄호와 쉼표를 제거하고 숫자만 포함된 문자열 생성
        String[] numbers = input.replaceAll("[\\[\\],]", "").split("\\s+");
        int[] array = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            try {
                array[i] = Integer.parseInt(numbers[i]);
            } catch (NumberFormatException e) {
                System.out.println("유효하지 않은 입력입니다: " + numbers[i]);
                System.exit(1);
            }
        }
        return array;
    }

    // 선택 정렬 알고리즘 구현
    private static void sortArray(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < n; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            int temp = arr[minIndex];
            arr[minIndex] = arr[i];
            arr[i] = temp;
        }
    }

    // 평균값 계산
    private static double calculateAverage(int[] arr) {
        int sum = 0;
        for (int num : arr) {
            sum += num;
        }
        return (double) sum / arr.length;
    }

    // 중앙값 계산
    private static double calculateMedian(int[] arr) {
        int n = arr.length;
        if (n % 2 == 0) {
            // 배열의 길이가 짝수인 경우 중앙에 위치한 두 값의 평균 사용
            return (double) (arr[n / 2 - 1] + arr[n / 2]) / 2;
        } else {
            // 배열의 길이가 홀수인 경우 중앙값 반환
            return arr[n / 2];
        }
    }
}
