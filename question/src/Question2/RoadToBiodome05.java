package Question2;
import Question1.HelloBiodome04;

public class RoadToBiodome05 {
    public static int[] quicksort(String[] args) {
        //init
        String input = "";
        int[] arr = {};

        //input args in input
        for (String str : args) {
            input += str + " ";
        }
        input = input.trim();

        if (countChar(input, '[') != countChar(input, ']')) {
            System.out.println("제대로 된 집단을 입력해주세요.");
            return null;
        }

        arr = parseArray(input);
        if (arr == null) {
            System.out.println("제대로된 값을 입력해주세요.");
            return null;
        }

        if (!check_value(arr)) {
            System.out.println("입력 값의 범위는 0 < value < 1000 입니다.");
            return null;
        }
        q_sort(arr);

        return arr;
    }

    private static int countChar(String input, char ch) {
        int count = 0;

        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == ch) {
                count++;
            }
        }

        return count;
    }

    private static int[] parseArray(String input) {
        // 대괄호와 쉼표를 제거하고 숫자만 포함된 문자열 생성
        String[] numbers = input.replaceAll("[\\[\\],]", "").split("\\s+");
        int[] array = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            if (HelloBiodome04.isNumberic(numbers[i]))
                array[i] = Integer.parseInt(numbers[i]);
            else
                return null;
        }
        return array;
    }

    private static void q_sort(int[] arr) {
        if (arr == null || arr.length == 0) {
            return;
        }
        sort(arr, 0, arr.length - 1);
    }

    private static void sort(int[] arr, int low, int high) {
        if (low >= high) {
            return;
        }
        int pivot = arr[(low + high) / 2];
        int i = low, j = high;
        while (i <= j) {
            while (arr[i] < pivot) {
                i++;
            }
            while (arr[j] > pivot) {
                j--;
            }
            if (i <= j) {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            }
        }
        if (low < j) {
            sort(arr, low, j);
        }
        if (high > i) {
            sort(arr, i, high);
        }
    }

    private static boolean check_value(int[] arr) {
        for (int value : arr) {
            if (value < 0 || value > 1000) {
                return false;
            }
        }

        return true;
    }
}
