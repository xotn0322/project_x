package Question2;

import Question1.HelloBiodome04;

import java.util.Arrays;
import java.util.Stack;

public class RoadToBiodome06 {
    public static float[] meanMedian(String[] args) {
        //init
        String[] input;
        float[] result = new float[10];

        input = split_array(args);

        int[] arr1 = parseArray(input[0]);
        int[] arr2 = parseArray(input[1]);

        if (!check_value(arr1) || !check_value(arr2)) {
            System.out.println("입력 값의 범위는 0 < value < 1000 입니다.");
            return null;
        }

        result[0] = getMean(arr1, arr2);
        result[1] = findMedianSortedArrays(arr1, arr2);

        return result;
    }

    private static float getMean(int[] arr1, int[] arr2) {
        float result = 0;
        int sum1 = 0;
        int sum2 = 0;
        float total_lenght = 0;

        for (int value : arr1) {
            sum1 += value;
        }
        for (int value : arr2) {
            sum2 += value;
        }

        total_lenght = arr1.length + arr2.length;

        result = (sum1 + sum2) / total_lenght;

        return result;
    }

    private static boolean check_value(int[] arr) {
        for (int value : arr) {
            if (value < 0 || value > 1000) {
                return false;
            }
        }

        return true;
    }



    public static float findMedianSortedArrays(int[] nums1, int[] nums2) {
        int totalLength = nums1.length + nums2.length;
        if (totalLength % 2 == 1) {
            return findKthSmallest(nums1, 0, nums2, 0, totalLength / 2 + 1);
        } else {
            return (findKthSmallest(nums1, 0, nums2, 0, totalLength / 2) +
                    findKthSmallest(nums1, 0, nums2, 0, totalLength / 2 + 1)) / 2.0f;
        }
    }

    private static int findKthSmallest(int[] nums1, int start1, int[] nums2, int start2, int k) {
        if (start1 >= nums1.length) {
            return nums2[start2 + k - 1];
        }
        if (start2 >= nums2.length) {
            return nums1[start1 + k - 1];
        }
        if (k == 1) {
            return Math.min(nums1[start1], nums2[start2]);
        }

        int mid1 = start1 + k / 2 - 1 < nums1.length ? nums1[start1 + k / 2 - 1] : Integer.MAX_VALUE;
        int mid2 = start2 + k / 2 - 1 < nums2.length ? nums2[start2 + k / 2 - 1] : Integer.MAX_VALUE;

        if (mid1 < mid2) {
            return findKthSmallest(nums1, start1 + k / 2, nums2, start2, k - k / 2);
        } else {
            return findKthSmallest(nums1, start1, nums2, start2 + k / 2, k - k / 2);
        }
    }

    public static String[] split_array(String[] args) {
        Stack<Character> stack = new Stack<Character>();
        int input_num = 0;
        String[] input = new String[100];
        Arrays.fill(input, "");

        for (String str : args) {
            if (str.contains("[")) {
                stack.push('[');
            }
            input[input_num] += str + " ";
            if (str.contains("]")) {
                if (!stack.isEmpty() && stack.peek() == '[') {
                    stack.pop();
                    input_num++;
                } else {
                    System.out.println("제대로된 집단을 입력해주세요.");
                    break;
                }
            }
        }

        return input;
    }

    private static int[] parseArray(String input) {
        // 대괄호와 쉼표를 제거하고 숫자만 포함된 문자열 생성
        String[] numbers = input.replaceAll("[\\[\\],]", "").split("\\s+");
        int[] array = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            if (HelloBiodome04.isNumberic(numbers[i]))
                array[i] = Integer.parseInt(numbers[i]);
            else
                return null;
        }
        return array;
    }
}
