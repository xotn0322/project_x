package Question2;

import Question1.HelloBiodome04;
import java.util.Arrays;

public class RoadToBiodome07 {
    private static int count = 0;

    public static String[] sortAnimal(String[] args) {
        //init
        String input = "";
        Animal[] arr = {};

        //input args in input
        for (String str : args) {
            input += str + " ";
        }
        input = input.trim();

        arr = parseArray(input);
        sortArray(arr);
        arrayAscending(arr);

        String[] result = new String[arr.length];
        for (int i = 0; i < count; i++) {
            result[i] = arr[i].getName();
        }

        return result;
    }

    private static Animal[] parseArray(String input) {
        // 대괄호와 쉼표를 제거하고 숫자만 포함된 문자열 생성
        String[] animals = input.replaceAll("[\\[\\],]", "").split("\\s+");
        Animal[] array = new Animal[animals.length];
        for (int i = 0; i < animals.length; i++) {
            for (int j = 0; j <= i; j++) {
                if (array[j] == null) {
                    array[j] = new Animal();
                    array[j].setName(animals[i]);
                    array[j].upCheck();
                    count++;
                    break;
                } else if (array[j].getName().equals(animals[i])) {
                    array[j].upCheck();
                    break;
                }
            }
        }
        return array;
    }

    // 선택 정렬 알고리즘 구현
    private static void sortArray(Animal[] arr) {
        int n = count;
        for (int i = 0; i < n - 1; i++) {
            int maxIndex = i;
            for (int j = i + 1; j < n; j++) {
                if (arr[j].getCheck() > arr[maxIndex].getCheck()) {
                    maxIndex = j;
                }
            }
            Animal temp = arr[maxIndex];
            arr[maxIndex] = arr[i];
            arr[i] = temp;
        }
    }

    //Animal.name에 따라 오름차순 정렬
    private static void arrayAscending(Animal[] arr) {
        for (int i = 0; i < count-1; i++) {
            Integer c = arr[i].getCheck();
            if (c.compareTo(arr[i+1].getCheck()) == 0) {
                if (arr[i].getName().compareTo(arr[i+1].getName()) > 0) { //0초과일 때, arr[i]가 arr[i+1]보다 사전적으로 먼저이다. (String.compareto()는 기존값과 비교값의 차이값을 반환한다.)
                    Animal temp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;
                }
            }
        }
    }
}

class Animal {
    private String name; //이름
    private int check;//입력된 횟수

    Animal() {
        name = "";
        check = 0;
    }

    public String getName() {
        return this.name;
    }
    public int getCheck() {
        return this.check;
    }
    public void upCheck() {
        this.check++;
    }
    public void setName(String name) {
        this.name = name;
    }

}
