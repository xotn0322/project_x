package Question2;
import Question1.HelloBiodome04;

public class RoadToBiodome08 {
    public static void Queue(String[] args) {
        if (args.length > 0) {
            //init
            int[] arr = {};
            Queue queue = new Queue();

            //enqueue
            for (String value : args) {
                if (!HelloBiodome04.isNumberic(value)) {
                    System.out.println("숫자를 입력해주세요.");
                    return;
                }
                else {
                    queue.enqueue(Integer.parseInt(value));
                }
            }

            //dequeue
            while(true) {
                int value = queue.dequeue();
                if (value == -1) {
                    System.out.println("모든 요청이 처리되었습니다.");
                    break;
                }
                else {
                    System.out.println("자원 " + value + "을 제공했습니다.");
                }
            }
        }
        else {
            System.out.println("값을 입력해주세요.");
            return;
        }
    }
}

class Queue {
    final int size = 100;
    int[] data;
    private static int front;
    private static int rear;

    Queue() {
        data = new int[size];
        front = 0;
        rear = 0;
    }

    public void enqueue(int value) {
        if (!isFull()) {
            data[front] = value;
            front++;
        }
        else {
            System.out.println("큐가 가득 찼습니다.");
            return;
        }
    }
    public int dequeue() {
        if (!isEmpty()) {
            int result = data[rear];
            rear++;
            return result;
        }
        else {
            System.out.println("큐가 비어있습니다.");
            return -1;
        }
    }
    public int peek() {
        if (!isEmpty()) {
            return data[rear];
        }
        else {
            System.out.println("큐가 비어있습니다.");
            return -1;
        }
    }
    public boolean isEmpty() {
        return front == rear;
    }
    public boolean isFull() {
        return front == size;
    }
}
