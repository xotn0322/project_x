package Question2;
import java.util.LinkedList;
import java.util.Queue;

public class RoadToBiodome09 {
    public static void FindRoad(String[] args) {
        final int mapSize = args.length;

        if (args.length == 0) {
            System.out.println("지형 정보를 입력하세요.");
            return;
        }

        // 입력된 지형 정보를 2차원 배열로 변환
        String[] rows = new String[mapSize];
        for (int i = 0; i < args.length; i++) {
            rows[i] = args[i];
        }

        int N = rows.length;
        int M = rows[0].length();
        int[][] cave = new int[N][M];

        for (int i = 0; i < N; i++) {
            if (rows[i].length() != M) {
                System.out.println("올바른 2차원 배열 형식이 아닙니다.");
                return;
            }
            for (int j = 0; j < M; j++) {
                if (rows[i].charAt(j) == '0' || rows[i].charAt(j) == '1' || rows[i].charAt(j) == '2') {
                    cave[i][j] = rows[i].charAt(j) - '0';
                } else {
                    System.out.println("입력값은 0과 1과 2로만 구성되어야 합니다.");
                    return;
                }
            }
        }

        int shortestPath = bfs(cave, N, M);
        if (shortestPath == -1) {
            System.out.println("입구에서 출구로 연결된 안전한 경로가 없습니다.");
        } else {
            System.out.println(shortestPath);
        }
    }

    // BFS 알고리즘을 사용하여 최단 경로를 계산하는 함수
    private static int bfs(int[][] cave, int N, int M) {
        if (cave[0][0] == 0 || cave[N - 1][M - 1] == 0) {
            return -1;
        }

        boolean[][] visited = new boolean[N][M];
        //상하좌우 시 각 인덱스의 변화
        //ex) 상 = dx[1],dy[1] = (x+1, y)
        int[] dx = {1, -1, 0, 0};
        int[] dy = {0, 0, 1, -1};

        Queue<int[]> queue = new LinkedList<>();
        queue.add(new int[]{0, 0, 0});  // (x, y, distance)
        visited[0][0] = true;

        while (!queue.isEmpty()) {
            int[] current = queue.poll();
            int x = current[0];
            int y = current[1];
            int dist = current[2];

            if (x == N - 1 && y == M - 1) {
                return dist;
            }

            for (int i = 0; i < 4; i++) {
                int nx = x + dx[i]; //이동한 x좌표
                int ny = y + dy[i]; //이동한 y좌표

                if (nx >= 0 && ny >= 0 && nx < N && ny < M && cave[nx][ny] == 1 && !visited[nx][ny]) {
                    visited[nx][ny] = true;
                    queue.add(new int[]{nx, ny, dist + 1});
                }
            }
        }

        return -1;  // 출구로 가는 경로가 없는 경우
    }
}