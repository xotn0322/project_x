package Question2;
import java.util.*;

public class RoadToBiodome10 {
    public static void matchPlant(String[] args) {
        if (args.length == 0) {
            System.out.println("식물간 연관관계를 입력하세요.");
            return;
        }

        String input = "";
        for (String value : args) {
            input += value + " ";
        }
        String[] relations = input.split(" ");

        // 최대 100개의 식물
        int maxPlants = 100;
        boolean[][] graph = new boolean[maxPlants + 1][maxPlants + 1];

        // 그래프 생성
        try {
            for (String relation : relations) {
                String[] plants = relation.split(",");
                if (plants.length != 2) {
                    throw new IllegalArgumentException();
                }

                int plant1 = Integer.parseInt(plants[0]);
                int plant2 = Integer.parseInt(plants[1]);

                if (plant1 < 1 || plant1 > maxPlants || plant2 < 1 || plant2 > maxPlants) {
                    throw new IllegalArgumentException();
                }

                graph[plant1][plant2] = true;
                graph[plant2][plant1] = true;
            }
        } catch (NumberFormatException e) {
            System.out.println("입력 값에 문자가 포함되어 있습니다.");
            return;
        } catch (IllegalArgumentException e) {
            System.out.println("식물의 범위를 벗어난 숫자가 포함되어 있거나 잘못된 입력 형식입니다.");
            return;
        }

        boolean[] visited = new boolean[maxPlants + 1];
        int groupCount = 0;

        for (int i = 1; i <= maxPlants; i++) {
            if (!visited[i] && hasConnection(graph, i, maxPlants)) {
                dfs(graph, visited, i, maxPlants);
                groupCount++;
            }
        }

        System.out.println(groupCount);
    }

    // DFS 알고리즘
    private static void dfs(boolean[][] graph, boolean[] visited, int plant, int maxPlants) {
        Stack<Integer> stack = new Stack<>();
        stack.push(plant);

        while (!stack.isEmpty()) {
            int current = stack.pop();

            if (!visited[current]) {
                visited[current] = true;

                for (int i = 1; i <= maxPlants; i++) {
                    if (graph[current][i] && !visited[i]) {
                        stack.push(i);
                    }
                }
            }
        }
    }

    // 연결이 있는지 확인하는 함수
    private static boolean hasConnection(boolean[][] graph, int plant, int maxPlants) {
        for (int i = 1; i <= maxPlants; i++) {
            if (graph[plant][i]) {
                return true;
            }
        }
        return false;
    }
}
