package Question3;

import java.util.List;

public class BiodomeFamily01 {
    public static void access() {
        LifeNest list = new LifeNest();

        Organism penguin = new Organism("펭귄", "동물", "남극");
        Organism koala = new Organism("코알라", "동물", "호주");
        Organism cactus = new Organism("선인장", "식물", "사막");
        Organism mint = new Organism("페퍼민트", "식물", "정원");

        list.addOrganism(penguin);
        list.addOrganism(koala);
        list.addOrganism(cactus);
        list.addOrganism(mint);

        list.getAllOrganism();

        list.subtractOrganism(koala);
        list.subtractOrganism(cactus);

        list.getAllOrganism();
    }
}

class Organism {
    private final String name;
    private final String species;
    private final String habitat;

    //init
    Organism() {
        this.name = "";
        this.species = "";
        this.habitat = "";
    }
    Organism(String name, String Species, String habitat) {
        this.name = name;
        this.species = Species;
        this.habitat = habitat;
    }

    //get method
    public String getName() {
        return name;
    }
    public String getSpecies() {
        return species;
    }
    public String getHabitat() {
        return habitat;
    }

    public void displayInfo() {
        System.out.println(getName() + ", " + getSpecies() + ", " + getHabitat());
    }
}

class LifeNest {
    List<Organism> organismList;

    LifeNest() {

    }

    //method
    void addOrganism(Organism organism) {
        organismList.add(organism);
        System.out.println("[LifeNest] " + organism.getName() + "이 추가되었습니다.");
    }
    void subtractOrganism(Organism organism) {
        if(!organismList.isEmpty()) {
            organismList.remove(organism);
            System.out.println("[LifeNest] " + organism.getName() + "이 추가되었습니다.");
        }
    }

    //getMethod
    void getAllOrganism() {
        int i = 1;
        for (Organism object : organismList) {
            System.out.print(i + ".");
            object.displayInfo();
        }
    }
}