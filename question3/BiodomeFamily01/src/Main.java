public class Main {
    public static void main(String[] args) {
        LifeNest list = new LifeNest();

        Organism penguin = new Organism("펭귄", "동물", "남극");
        Organism koala = new Organism("코알라", "동물", "호주");
        Organism cactus = new Organism("선인장", "식물", "사막");
        Organism mint = new Organism("페퍼민트", "식물", "정원");

        list.addOrganism(penguin);
        list.addOrganism(penguin);
        list.addOrganism(koala);
        list.addOrganism(cactus);
        list.addOrganism(mint);

        list.getAllOrganism();

        list.searchOrganismByName("펭귄");

        list.subtractOrganism(koala);
        list.subtractOrganism(koala);
        list.subtractOrganism(cactus);

        list.getAllOrganism();
    }
}