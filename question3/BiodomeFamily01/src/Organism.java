public class Organism {
    private final String name;
    private final String species;
    private final String habitat;

    //init
    Organism(String name, String Species, String habitat) {
        this.name = name;
        this.species = Species;
        this.habitat = habitat;
    }

    //get method
    public String getName() {
        return name;
    }
    public String getSpecies() {
        return species;
    }
    public String getHabitat() {
        return habitat;
    }

    public void displayInfo() {
        System.out.println(getName() + ", " + getSpecies() + ", " + getHabitat());
    }
}
