import java.util.ArrayList;
import java.util.List;

public class LifeNest {
    private List<Organism> organismList;

    LifeNest() {
        organismList = new ArrayList<>();
    }

    //method
    void addOrganism(Organism organism) {
        if (!organismList.contains(organism)) {
            organismList.add(organism);
            System.out.println("[LifeNest] " + organism.getName() + "(이)가 추가되었습니다.");
        }
        else {
            System.out.println("이미 존재합니다.");
        }
    }
    void subtractOrganism(Organism organism) {
        if(!organismList.isEmpty()) {
            if (!organismList.contains(organism)){
                System.out.println("존재하지 않습니다.");
            }
            else {
                organismList.remove(organism);
                System.out.println("[LifeNest] " + organism.getName() + "(이)가 삭제되었습니다.");
            }
        }
    }

    //getMethod
    void getAllOrganism() {
        int i = 1;
        for (Organism object : organismList) {
            System.out.print(i + ".");
            object.displayInfo();
            i++;
        }
    }

    void searchOrganismByName(String name) {
        for (Organism object : organismList) {
            if (object.getName().equals(name)) {
                System.out.println(object.getName() + "(은)는 " + object.getSpecies() + "이며 " + object.getHabitat() + "에 서식합니다.");
                break;
            }
            else {
                System.out.println("존재하지 않습니다.");
                break;
            }
        }
    }

    //setMoethod
    /**생물의 서식지 변경*/
    void setOrganismHabitat(Organism organism, String habitat) {
        organism.setHabitat(habitat);
    }
}
