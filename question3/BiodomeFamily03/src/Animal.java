public class Animal extends Organism{
    private String digest;
    private String feed;

    //init
    Animal(String name, String species, String habitat, String digest, String feed) {
        super(name, species, habitat);
        this.digest = digest;
        this.feed = feed;
    }

    //getter method
    public String getDigest() {
        return digest;
    }
    public String getFeed() {
        return feed;
    }

    //setter method
    public void setDigest(String digest) {
        this.digest = digest;
    }
    public void setFeed(String feed) {
        this.feed = feed;
    }

    //Override
    @Override
    public void displayInfo() {
        System.out.println(getName() + ", " + getSpecies() + ", " + getHabitat() + ", " + getDigest() + ", " + getFeed());
    }
}
