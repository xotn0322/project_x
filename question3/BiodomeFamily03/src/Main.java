public class Main {
    public static void main(String[] args) {
        LifeNest list = new LifeNest();

        Animal penguin = new Animal("펭귄", "동물", "남극", "육식", "물고기");
        Animal koala = new Animal("코알라", "동물", "호주", "초식", "유칼립투스");
        Plant cactus = new Plant("선인장", "식물", "사막", "11월", "열매 있음");
        Plant mint = new Plant("페퍼민트", "식물", "정원", "7월", "열매 없음");

        list.addOrganism(penguin);
        list.addOrganism(penguin);
        list.addOrganism(koala);
        list.addOrganism(cactus);
        list.addOrganism(mint);

        System.out.println();

        list.getAllOrganism();

        System.out.println();

        list.subtractOrganism(koala);
        list.subtractOrganism(koala);
        list.subtractOrganism(cactus);

        System.out.println();

        list.getAllOrganism();
    }
}