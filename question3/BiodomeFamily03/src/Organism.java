public class Organism {
    private String name;
    private String species;
    private String habitat;

    //init
    Organism(String name, String species, String habitat) {
        this.name = name;
        this.species = species;
        this.habitat = habitat;
    }

    //getter method
    public String getName() {
        return name;
    }
    public String getSpecies() {
        return species;
    }
    public String getHabitat() {
        return habitat;
    }

    //setter method
    public void setName(String name) {
        this.name = name;
        System.out.println("이름이 " + name + "으로 변경되었습니다.");
    }
    public void setSpecies(String species) {
        this.species = species;
        System.out.println("종이 " + species + "으로 변경되었습니다.");
    }
    public void setHabitat(String habitat) {
        this.habitat = habitat;
        System.out.println(getName() + " 서식지가 " + habitat + "으로 변경되었습니다.");
    }

    public void displayInfo() {
        System.out.println(getName() + ", " + getSpecies() + ", " + getHabitat());
    }
}
