public class Plant extends Organism{
    private String floweringPeriod;
    private String fruit;

    //init
    Plant(String name, String species, String habitat, String floweringPeriod, String Fruit) {
        super(name, species, habitat);
        this.floweringPeriod = floweringPeriod;
        this.fruit = Fruit;
    }

    //getter method
    public String getFloweringPeriod() {
        return floweringPeriod;
    }
    public String getFruit() {
        return fruit;
    }

    //setter method
    public void setFloweringPeriod(String floweringPeriod) {
        this.floweringPeriod = floweringPeriod;
    }
    public void setFruit(String fruit) {
        this.fruit = fruit;
    }

    @Override
    public void displayInfo() {
        System.out.println(getName() + ", " + getSpecies() + ", " + getHabitat() + ", " + getFloweringPeriod() + ", " + getFruit());
    }
}
