public class EnergyManager{
    private EnergySource[] energySources;
    private int totalEnergy;

    EnergyManager() {
        totalEnergy = 0;
    }

    public int getTotalEnergy() {
        return totalEnergy;
    }

    public void addTotalEnergy(int totalEnergy) {
        this.totalEnergy += totalEnergy;
    }

    public void abstractTotalEnergy(int used) {
        this.totalEnergy -= used;
    }
}