abstract class EnergySource{
    private String sourceName;
    private int energyAmount;

    public EnergySource() {
        energyAmount = 0;
    }

    public void useEnergy(EnergyManager manager, int used) {
        if (manager.getTotalEnergy() >= used) {
            manager.abstractTotalEnergy(used);
            System.out.println(sourceName + "를 " + used + " 사용했습니다.");
        }
        else {
            System.out.println("에너지가 부족해 " + sourceName + " " + used + "를 사용할 수 없습니다.");
        }
    }
    abstract void produceEnergy(int Fuel);

    //getter
    public String getSourceName() {
        return sourceName;
    }
    public int getEnergyAmount() {
        return energyAmount;
    }

    //setter
    public void setSourceName(String name) {
        sourceName = name;
    }
    public void addEnergyAmount(int energy) {
        energyAmount += energy;
    }
}


class solarForce extends EnergySource{
    solarForce() {
        super.setSourceName("태양광 에너지");
    }

    void produceEnergy(int time) {
        int energy = time * 10;
        super.addEnergyAmount(energy);
        System.out.println(super.getSourceName() + "를 " + energy + " 생성했습니다.");
    }
}

class windForce extends EnergySource{
    windForce() {
        super.setSourceName("풍력 에너지");
    }

    void produceEnergy(int velocity) {
        int energy = velocity * 5;
        super.addEnergyAmount(velocity * 5);
        System.out.println(super.getSourceName() + "를 " + energy + " 생성했습니다.");
    }
}

class earthForce extends EnergySource{
    earthForce() {
        super.setSourceName("지열 에너지");
    }

    void produceEnergy(int temp) {
        int energy = (temp * 5) + 20;
        super.addEnergyAmount((temp * 5) + 20);
        System.out.println(super.getSourceName() + "를 " + energy + " 생성했습니다.");
    }
}