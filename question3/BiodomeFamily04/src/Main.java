public class Main {
    public static void main(String[] args) {
        solarForce solar = new solarForce();
        windForce wind = new windForce();
        earthForce earth = new earthForce();
        EnergyManager manager = new EnergyManager();

        solar.produceEnergy(1);
        wind.produceEnergy(4);
        earth.produceEnergy(2);

        manager.addTotalEnergy(solar.getEnergyAmount());
        manager.addTotalEnergy(wind.getEnergyAmount());
        manager.addTotalEnergy(earth.getEnergyAmount());

        solar.useEnergy(manager,30);
        wind.useEnergy(manager,30);
        earth.useEnergy(manager,30);

        System.out.println("남은 에너지 : " + manager.getTotalEnergy());
    }
}